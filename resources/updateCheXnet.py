import werkzeug
import os
from flask_apispec import doc, marshal_with, use_kwargs
from flask import current_app as app
from flask_apispec.views import MethodResource
from flask_restful import Resource, reqparse
from marshmallow import Schema, fields
from datetime import datetime
from uuid import uuid4
import json

import pydicom
from common.config.config import Config as cfg

parser = reqparse.RequestParser()
parser.add_argument('image', type=werkzeug.datastructures.FileStorage, required=True, location='files')
parser.add_argument('x1', type=int, required=True, help="Coordinate x1 cannot be blank!")
parser.add_argument('y1', type=int, required=True, help="Coordinate y1 cannot be blank!")
parser.add_argument('x2', type=int, required=True, help="Coordinate x2 cannot be blank!")
parser.add_argument('y2', type=int, required=True, help="Coordinate y2 cannot be blank!")
parser.add_argument('x3', type=int, required=True, help="Coordinate x3 cannot be blank!")
parser.add_argument('y3', type=int, required=True, help="Coordinate y3 cannot be blank!")
parser.add_argument('x4', type=int, required=True, help="Coordinate x4 cannot be blank!")
parser.add_argument('y4', type=int, required=True, help="Coordinate y4 cannot be blank!")
parser.add_argument('model', type=str, required=True, help="Model Name cannot be blank!")
parser.add_argument('modelVersion', type=str, required=True, help="Model Version Number cannot be blank!")
parser.add_argument('annotationText', type=str, required=True, help="Annotation Text cannot be blank!")

class ModelUpdateRequestSchema(Schema):
    image = fields.Raw(description="DICOM Image")
    x1 = fields.Number(description="x1 co-ordinate of Bounding Box")
    y1 = fields.Number(description="y1 co-ordinate of Bounding Box")
    x2 = fields.Number(description="x2 co-ordinate of Bounding Box")
    y2 = fields.Number(description="y2 co-ordinate of Bounding Box")
    x3 = fields.Number(description="x3 co-ordinate of Bounding Box")
    y3 = fields.Number(description="y3 co-ordinate of Bounding Box")
    x4 = fields.Number(description="x4 co-ordinate of Bounding Box")
    y4 = fields.Number(description="y4 co-ordinate of Bounding Box")
    model = fields.String()
    modelVersion = fields.Number(description="Model Version Number")
    annotationText = fields.String()


class ModelUpdateResponseSchema(Schema):
    status = fields.String(default='Success', required=True)
    model = fields.String(required=True)
    modelVersion = fields.String(required=True)


class updateCheXnet(MethodResource, Resource):
    @doc(summary="Update Bounding Box model", description='Updates the Bounding Box model based on the image and co-ordinates', tags=['Update Model'])
    @use_kwargs(ModelUpdateRequestSchema)
    @marshal_with(ModelUpdateResponseSchema)
    def post(self):
        # Call few shot learning here
        args = parser.parse_args()
        image_file = args['image']
        model = "" + args['model']
        modelVersion = args['modelVersion']
        annotationText = args['annotationText']
        x1 = args['x1']
        y1 = args['y1']
        x2 = args['x2']
        y2 = args['y2']
        x3 = args['x3']
        y3 = args['y3']
        x4 = args['x4']
        y4 = args['y4']

        # Write image and data to disk
        filename = werkzeug.utils.secure_filename(image_file.filename)
        app.logger.debug("Recieved image - " + filename)

        app.logger.debug("Creating temp directory if it doesn't exist")
        os.makedirs("data/model_update", exist_ok=True)

        # Handle multiple POST requests with the same filename
        # at the exact same time
        unique_id = datetime.now().strftime('%Y%m-%d%H-%M%S-') + str(uuid4())
        filename = unique_id + "_" + filename

        base_dir = cfg.MODEL_UPDATE_DIR 

        temp_path = cfg.TEMP_DIR + filename
        app.logger.debug("Saving Temp DICOM file to disk - " + temp_path)
        image_file.save(temp_path)
        app.logger.debug("Saved Temp DICOM file to disk - " + temp_path)

        modality = ""

        if(pydicom.misc.is_dicom(temp_path)):
            modality = pydicom.read_file(temp_path).Modality + "/"
            base_dir = base_dir + modality

        os.makedirs(base_dir, exist_ok=True)
        dicom_file_path = base_dir + filename

        app.logger.debug("Saving file to disk - " + dicom_file_path)
        image_file.save(dicom_file_path)
        app.logger.debug("Saved file to disk - " + dicom_file_path)

        model_update_data = {
            "model" : model,
            "modelVersion" : modelVersion,
            "annotationText" : annotationText,
            "x1" : x1,
            "y1" : y1,
            "x2" : x2,
            "y2" : y2,
            "x3" : x3,
            "y3" : y3,
            "x4" : x4,
            "y4" : y4,
            "image" : filename
        }

        app.logger.debug(model_update_data)

        f = open(base_dir + unique_id + "_data.json", "x")
        f.write(json.dumps(model_update_data))
        f.close()

        return {"status": "Success", "data": [{'model': 'chexNet', 'version': '1.0'}]}, 202
